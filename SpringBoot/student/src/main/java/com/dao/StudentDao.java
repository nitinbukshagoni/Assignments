package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {
		@Autowired	
		StudentRepository studentRepo;
		public List<Student> getAllStudents() {
			List<Student> studentList = studentRepo.findAll();
			return studentList;
		}
		public Student getstudentById(int studId) {
			Student stud = new Student(0,"Student Not Found!!","","",0);
			Student student = studentRepo.findById(studId).orElse(stud);
			return student;
		}
		public Student getStudentByName(String studName) {
			return studentRepo.findByName(studName);
		}
		public Student addStudent(Student student) {
			return studentRepo.save(student);
		}	
}
