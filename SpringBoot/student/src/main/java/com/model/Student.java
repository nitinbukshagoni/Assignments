package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	
	@Id@GeneratedValue@Column(name="studentId")
	private int studentId;
	@Column(name="studentName")
	private String studentName;
	private String gender;
	private String course;
	@Column(name="studentfee")
	private double studentFee;

	public Student() {
		super();
	}

	public Student(int studentId, String studentName, String gender, String course, double studentFee) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.gender = gender;
		this.course = course;
		this.studentFee = studentFee;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public double getStudentFee() {
		return studentFee;
	}

	public void setStudentFee(double studentFee) {
		this.studentFee = studentFee;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", gender=" + gender + ", course="
				+ course + ", studentFee=" + studentFee + "]";
	}
}
