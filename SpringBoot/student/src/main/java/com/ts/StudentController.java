package com.ts;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	@Autowired
	StudentDao studentDao;
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {
		return studentDao.getAllStudents();
	}
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId) {
		Student student = studentDao.getstudentById(studId);
		return student;
	}
	@GetMapping("getStudentByName/{name}")
	public Student getStudentByName(@PathVariable("name") String studName) {
		return studentDao.getStudentByName(studName);
	}
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student) {
		return studentDao.addStudent(student);
	}

	// HardCoded
	@GetMapping("getstudent")
	public Student getProduct() {
		Student student = new Student();
		student.setStudentId(1);
		student.setStudentName("Nitin");
		student.setGender("Male");
		student.setCourse("CSE");
		student.setStudentFee(5000.00);
		return student;
	}
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		Student student1 = new Student(1, "Nitin", "Male", "CSE", 5000.00);
		Student student2 = new Student(2, "Dinesh", "Male", "CSE", 6000.00);
		Student student3 = new Student(3, "Biswal", "Male", "IT", 7000.00);
		
		List<Student> studentList = new ArrayList<Student>();
		studentList.add(student1);
		studentList.add(student2);
		studentList.add(student3);
		return studentList;
	}
}
